package ru.konovalov.tm.exeption.entity;

import ru.konovalov.tm.exeption.AbstractException;

public class RoleNotFoundException extends AbstractException {

    public RoleNotFoundException() {
        super("Error! Role not found...");
    }

}
