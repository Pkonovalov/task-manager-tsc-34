package ru.konovalov.tm.api.entity;

import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
