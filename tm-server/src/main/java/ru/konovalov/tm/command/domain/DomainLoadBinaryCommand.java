package ru.konovalov.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.Domain;
import ru.konovalov.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DomainLoadBinaryCommand extends AbstractDomainCommand {

    @Override
    public @Nullable String name() {
        return "data-load-bin";
    }

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String description() {
        return "Load binary data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
